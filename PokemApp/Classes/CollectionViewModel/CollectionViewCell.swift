//
//  CollectionViewCell.swift
//  PokemApp
//
//  Created by Luigi Mazzarella on 08/08/2020.
//  Copyright © 2020 Luigi Mazzarella. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
}
