//
//  PokemonCell.swift
//  PokemApp
//
//  Created by Luigi Mazzarella on 07/08/2020.
//  Copyright © 2020 Luigi Mazzarella. All rights reserved.
//

import UIKit

class PokemonCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var pokemonImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        // Initialization code
    }

}
