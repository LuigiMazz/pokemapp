//
//  PokemList.swift
//  PokemApp
//
//  Created by Luigi Mazzarella on 07/08/2020.
//  Copyright © 2020 Luigi Mazzarella. All rights reserved.
//

import Foundation

struct PokemonList: Codable {
    let count: Int
    let next: String?
    let previous: String?
    let results: [Result]
}

struct Result: Codable {
    let name: String
    let url: String
}
