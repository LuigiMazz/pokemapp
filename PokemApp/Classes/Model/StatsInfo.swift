//
//  StatsInfo.swift
//  PokemApp
//
//  Created by Luigi Mazzarella on 08/08/2020.
//  Copyright © 2020 Luigi Mazzarella. All rights reserved.
//

import Foundation

struct StatsInfo {
    let name: String
    let value: Int
}
