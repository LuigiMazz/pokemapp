//
//  PokemonData.swift
//  PokemApp
//
//  Created by Luigi Mazzarella on 07/08/2020.
//  Copyright © 2020 Luigi Mazzarella. All rights reserved.
//

/*import Foundation

struct PokemonData: Codable {
    let abilities: [Ability]
    let base_experience: Int
    let forms: [Basic]
    let game_indices: [GameIndice]
    let height: Int
    let held_items: [Any]?
    let id: Int
    let is_default: Bool
    let location_area_encounters: String
    let moves: [Move]
    let name: String
    let order: Int
    let species: Basic
    let sprites: Sprite
    let stats: [Stat]
    let types: [Type]
    let weight: Int
}

struct Ability: Codable {
    let ability: Basic
    let is_hidden: Bool
    let slot: Int
}

struct Basic: Codable {
    let name: String
    let url: String
}

struct Form: Codable {
    let name: String
    let url: String
}

struct GameIndice: Codable {
    let game_index: Int
    let version: Basic
}

struct Version: Codable {
    let name: String
    let url: String
}

struct Move: Codable {
    let move: Basic
    let version_group_details: [VersioneGroupDetail]
}

struct MoveData: Codable {
    let name: String
    let url: String
}

struct VersioneGroupDetail: Codable {
    let level_learned_at: Int
    let move_learn_method: Basic
    let version_group: Basic
}

struct Sprite: Codable {
    let back_default: String?
    let back_female: String?
    let back_shiny: String?
    let back_shiny_female: String?
    let front_default: String?
    let front_female: String?
    let front_shiny: String?
    let front_shiny_female: String?
    let other: Other
    let versions: VersionData
}

struct Other: Codable {
    let dream_world: DreamWorld
}

struct DreamWorld: Codable{
    let front_default: String?
    let front_female: String?
}


*/
