//
//  HTTPManager.swift
//  PokemApp
//
//  Created by Luigi Mazzarella on 07/08/2020.
//  Copyright © 2020 Luigi Mazzarella. All rights reserved.
//

import Foundation

class HTTPManager {
    
    //MARK: - Static & Variables
    static let shared = HTTPManager()
    
    static let baseURL = "https://pokeapi.co/api/v2/pokemon"
    
    //MARK: - Typealias
    typealias HTTPManagerResponse = (Bool,Any?) -> Void //Params: Success,Data/Error
    typealias HTTPResponse = (Bool,PokemonList?) -> Void //Params: Success,Data/Error
    typealias HTTPPokemonResponse = (Bool, PokemonAPI?) -> Void //Params: Success,Data/Error
    
    //MARK: - ExecuteRequest
    
    //Metodo generico per tutte le HTTPRequests (con completion closure generica)
    func executeRequest(request: URLRequest, completion: @escaping HTTPManagerResponse) {
        var req = request
        req.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let task = session.dataTask(with: req) { (data, response, error) in
            guard error == nil else {
                completion(false,error)
                return
            }
            
            let statusCode = (response != nil && response is HTTPURLResponse) ? (response as! HTTPURLResponse).statusCode : 400
            guard statusCode >= 200 && statusCode < 300 else {
                completion(false,data)
                return
            }
            
            completion(true,data)
        }
        task.resume()
    }
    //Eseguo ogni singola richiesta
    func getList(url: String, completion: @escaping HTTPResponse) {
        let url = URL(string:url)
        guard url != nil else {
            DispatchQueue.main.async {
                completion(false, nil)
            }
            return
        }
        var request = URLRequest(url: url!);
        request.httpMethod = "GET"
        
        executeRequest(request: request) { (success, data) in
            guard success else {
                DispatchQueue.main.async {
                    completion(false, nil)
                }
                return
            }
            guard data != nil && data is Data else {
                DispatchQueue.main.async {
                    completion(false,nil)
                }
                return
            }
            do {
                let decoder = JSONDecoder()
                let pokemonResponse = try decoder.decode(PokemonList.self, from: data as! Data)
                DispatchQueue.main.async {
                    completion(true, pokemonResponse)
                }
            
            } catch {
                DispatchQueue.main.async {
                    print(error)
                    completion(false, nil)
                }
            }
        }
    }
    
    func getPokemonData(pokemonURL: String, completion: @escaping HTTPPokemonResponse) {
        let url = URL(string:pokemonURL)
        guard url != nil else {
            DispatchQueue.main.async {
                completion(false, nil)
            }
            return
        }
        var request = URLRequest(url: url!);
        request.httpMethod = "GET"
        
        executeRequest(request: request) { (success, data) in
            guard success else {
                DispatchQueue.main.async {
                    completion(false, nil)
                }
                return
            }
            guard data != nil && data is Data else {
                DispatchQueue.main.async {
                    completion(false,nil)
                }
                return
            }
            do {
                let decoder = JSONDecoder()
                let dataResponse = try decoder.decode(PokemonAPI.self, from: data as! Data)
                DispatchQueue.main.async {
                    completion(true, dataResponse)
                }
            
            } catch {
                DispatchQueue.main.async {
                    print(error)
                    completion(false, nil)
                }
            }
        }
    }
}
