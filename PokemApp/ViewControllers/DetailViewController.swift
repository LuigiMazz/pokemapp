//
//  DetailViewController.swift
//  PokemApp
//
//  Created by Luigi Mazzarella on 07/08/2020.
//  Copyright © 2020 Luigi Mazzarella. All rights reserved.
//

import UIKit
import SDWebImage

class DetailViewController: UIViewController {

    //MARK: - IBOutlet
    
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Variables
    var namePokemon = ""
    var pokemonType = ""
    var urlString = ""
    var pokemonStats : [StatsInfo] = []
    static var imageURLString: [String] = []
    //MARK: - View Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        getPokemonInfo()
    }
    
    //MARK: - Actions
    func getPokemonInfo() {
        showSpinner(onView: self.view, alpha: 0.5)
        HTTPManager.shared.getPokemonData(pokemonURL: urlString) { (success, resp) in
            guard success && resp != nil else {
                return
                
            }
            
            //Salvo I dati del pokemon
            self.namePokemon = resp!.name
            self.pokemonType = resp!.types[0].type.name
            
            //Salvo URL delle img
            DetailViewController.imageURLString = [
                resp?.sprites.backDefault ?? "",
                resp?.sprites.frontDefault ?? "",
            ]
            
            //Salvo le statistiche dei pokemon
            self.pokemonStats = [
                StatsInfo(name: "", value: 0),
                StatsInfo(name: "", value: 0),
                //Inizio Array
                StatsInfo(name: "Height", value: resp!.height),
                StatsInfo(name: "Weight", value: resp!.weight),
                StatsInfo(name: "HP", value: resp!.stats[0].baseStat),
                StatsInfo(name: "Attack", value: resp!.stats[1].baseStat),
                StatsInfo(name: "Defense", value: resp!.stats[2].baseStat),
                StatsInfo(name: "Special Attack", value: resp!.stats[3].baseStat),
                StatsInfo(name: "Special Defense", value: resp!.stats[4].baseStat),
                StatsInfo(name: "Speed", value: resp!.stats[5].baseStat),
            ]
            //Imposto il DataSource e il delegate della tableview per dare il tempo di caricare i dati
            self.tableView.dataSource = self
            self.tableView.delegate = self
            self.tableView.reloadData()
            
            self.removeSpinner()
        }
    }
  
}

    //MARK: - TableView Methods
extension DetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell", for: indexPath) as? DetailNameCell else {return UITableViewCell()}
                cell.nameLabel.text = namePokemon
                cell.typeLabel.text = pokemonType
            return cell
        } else if indexPath.row == 1 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "imageCell", for: indexPath) as? ImageTableViewCell else {return UITableViewCell()}
            cell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, flowDelegate: self, forRow: indexPath.row)

            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "statsCell", for: indexPath) as? StatsCell else {return UITableViewCell()}
            cell.statsLabel.text = pokemonStats[indexPath.row].name
            cell.valueLabel.text = "\(pokemonStats[indexPath.row].value)"
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 100.0
        } else if indexPath.row == 1 {
            return 280.0
        } else {
            return 75.0
        }
    }
}

    //MARK: - CollectionView Methods
extension UIViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return DetailViewController.imageURLString.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? CollectionViewCell else {return UICollectionViewCell()}
        cell.imageView?.sd_setImage(with: URL(string: DetailViewController.imageURLString[indexPath.item]), completed: nil)
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 275, height: 275)
    }
    
    
}
