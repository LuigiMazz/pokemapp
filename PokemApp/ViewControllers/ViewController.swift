//
//  ViewController.swift
//  PokemApp
//
//  Created by Luigi Mazzarella on 07/08/2020.
//  Copyright © 2020 Luigi Mazzarella. All rights reserved.
//

import UIKit


class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    static var imageList : [String] = []
    var pokemonList: [Result] = []
    var counter = 0
    var nextPage = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        getPokemList(pageURL: HTTPManager.baseURL)
    }
    
    func getPokemList(pageURL: String) {
        HTTPManager.shared.getList(url: pageURL, completion: { (success, resp) in
            guard success && resp != nil else {
                return
            }
            self.pokemonList.append(contentsOf: resp!.results)
            self.nextPage = resp!.next ?? ""
            self.tableView.reloadData()
        })
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemonList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PokemonCell", for: indexPath) as? PokemonCell else {return UITableViewCell()}
        cell.nameLabel.text = pokemonList[indexPath.row].name
        counter = indexPath.row + 1
    //    cell.pokemonImageView.sd_setImage(with: URL(string:imageList[indexPath.row]), completed: nil)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == pokemonList.count - 1 {
            getPokemList(pageURL: nextPage)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           //Passare solo l'url da poi fare la richiesta nella view successiva
        let urlString = pokemonList[indexPath.row].url
        let destinationVC = self.storyboard?.instantiateViewController(withIdentifier: "detailVC") as! DetailViewController
        destinationVC.urlString = urlString
        self.present(destinationVC, animated: true, completion: nil)

       }
}
